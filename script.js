/*
* Цикл ForEach работает с каэждым элементом массива, на котором он вызван, в порядке возрастания индекса.
* Он по очереди берет каждый из элементов, и совершает с ним действие, указанное в его callback функции.
* Его callback принимает три параметра: элемент массива, индекс этого элемента в массиве, и ссылку на сам массив.

*/

function filterBy (array, type) {
    const newArray = [];

    if (type === 'null') {
        type = 'object'
    }

    array.forEach(element => {
        if (typeof element !== type) {
            newArray.push(element)
        }
    });
    return newArray;
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
console.log(filterBy(['hello', 'world', 23, '23', null], 'null'));